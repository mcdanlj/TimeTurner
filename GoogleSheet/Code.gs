// Copyright Michael K Johnson
// License CC BY 4.0: https://creativecommons.org/licenses/by/4.0/

function getActivities(spreadsheet) {
  var activities = spreadsheet.getSheetByName('Activities');
  if (!activities) {
    activities = spreadsheet.insertSheet('Activities');
    activities.getRange('A1:C13').setValues([
        ['Name', 'Side', 'Comments'],
        ['Sleep', 'A', ''], // It's embossed...
        ['Task B', 'B', ''],
        ['Task C', 'C', ''],
        ['Task D', 'D', ''],
        ['Task E', 'E', ''],
        ['Task F', 'F', ''],
        ['Task G', 'G', ''],
        ['Task H', 'H', ''],
        ['Task I', 'I', ''],
        ['Task J', 'J', ''],
        ['Task K', 'K', ''],
        ['Task L', 'L', ''],
      ]);
    activities.getRange('A1:C1').setFontWeight('bold');
    activities.getRange('A2:A13').setFontWeight('bold');
  }
  return activities;
}

function getToday(spreadsheet) {
  var today = spreadsheet.getSheetByName('Today');
  if (!today) {
    today = spreadsheet.insertSheet('Today');
  }
  return today;
}

function getData(spreadsheet) {
  var data = spreadsheet.getSheetByName('Data');
  if (!data) {
    data = spreadsheet.insertSheet('Data');
    data.getRange('A1').setValue('Today Duration').setFontWeight('bold');
  }
  return data;
}

function getSummary(spreadsheet) {
  var summary = spreadsheet.getSheetByName('Summary');
  if (!summary) {
    var summary = spreadsheet.getSheetByName('Sheet1');
    if (summary) {
      summary.setName('Summary');
    } else {
      summary = spreadsheet.insertSheet('Summary');
    }
    getActivities(spreadsheet); // for references
    summary.getRange('A1:B1').setValues([['Task', 'Side']]).setFontWeight('bold');
    var row;
    var formulas = [];
    for (row = 2; row <= 13; row++) {
      formulas.push(['Activities!A' + row, 'Activities!B' + row]);
    }
    summary.getRange('A2:B13').setFormulas(formulas);
    summary.getRange('A14').setValue('Total').setHorizontalAlignment('right').setFontWeight('bold');
    summary.hideColumn(summary.getRange('B1'));
  }
  return summary;
}


function mapFace(face, spreadsheet) {
  var activities = getActivities(spreadsheet);
  mapping = activities.getRange('A2:B13').getValues();
  activity = 'Unknown'
  mapping.forEach(function (tuple) {
    if (tuple[1] == face) {
      activity = tuple[0];
    }
  })
  Logger.log("Face " + face + " maps to " + activity);
  return activity;
}

function addTodayHeaders(today, todayDate) {
  var headers = [
    todayDate + ' Side',
    todayDate + ' Time',
    todayDate + ' Bat',]
  today.getRange('A1:C1').setValues([headers]).setFontWeight('bold');
}

function archiveColumns(spreadsheet, today, todayDate) {
  // Formulas will be modified by moves including insert/delete, so do all moves before setting any formulas
  var data = getData(spreadsheet);
  // insert columns to hold yesterday's data
  data.insertColumns(2, 3);
  // archive yesterday's data
  today.getRange('A1:B9999').moveTo(data.getRange('B1'));
  // clear the range so that appendRow will start back at the top
  today.getRange('A2:C9999').clear();
  addTodayHeaders(today, todayDate);

  var summary = getSummary(spreadsheet);
  summary.insertColumns(4, 1);
  // Add the new date headers
  summary.getRange('C1').setValue(todayDate).setFontWeight('bold');
  yesterday = data.getRange('B1').getValue().split(' ')[0]
  summary.getRange('D1').setValue(yesterday).setFontWeight('bold');
  data.getRange('D1').setValue(yesterday + ' Durations').setFontWeight('bold');

  // Reset the today duration formulas to point to the sheet, since they were changed by archiving yesterday's data
  var i;
  aDataFormulas = [];
  dDataFormulas = [];
  for (i=2; i<=9999; i++) {
    var row = i.toString();
    var next = (i+1).toString();
    aDataFormulas.push(["=IF(Today!B"+next+" > 0, Today!B"+next+"-Today!B"+row+", 0)"]);
    dDataFormulas.push(["=if(C"+next+">0,C"+next+"-C"+row+",IF(C"+row+">0,1-C"+row+",0))"]);
  }
  data.getRange('A2:A9999').setFormulas(aDataFormulas);
  data.getRange('D2:D9999').setFormulas(dDataFormulas);

  // Insert new summary formulas
  summaryFormulas = [];
  for (i=2; i<=13; i++) {
    var row = i.toString();
    summaryFormulas.push([
      // today
      "=SUMIF(Today!A$2:A$9999,$A"+row+",Data!A$2:A$9999)",
      // yesterday
      "=SUMIF(Data!B$2:B$9999,$A"+row+",Data!D$2:D$9999)"
    ]);
  }
  summaryFormulas.push([
    "=sum(C2:C13)",
    "=sum(D2:D13)"
  ]);
  summary.getRange('C2:D14').setFormulas(summaryFormulas).setNumberFormat('hh:mm:ss');
  summary.getRange('C14:D14').setFontWeight('italic')
}

function reportBatteryLevel(mv, spreadsheet) {
  var summary = getSummary(spreadsheet);
  if (mv < 3600) {
    summary.getRange('A1:D13').setBackground("red");
  } else if (mv < 3700) {
    summary.getRange('A1:D13').setBackground("yellow");
  } else {
    summary.getRange('A1:D13').setBackground("white");
  }
}

function addDataPoint(face, mv) {
  const date = new Date();
  const todayDate = date.toLocaleDateString('en-US');

  var spreadsheet = SpreadsheetApp.getActive();
  var today = getToday(spreadsheet);

  const existingDate = today.getRange('A1:A1').getValues()[0];
  Logger.log('today: %s existingDate: %s', todayDate, existingDate);

  if (!existingDate) {
    addTodayHeaders(today, todayDate);
  } else if (existingDate != todayDate + ' Side') {
    archiveColumns(spreadsheet, today, todayDate);
  }

  const timestamp = date.toTimeString().split(' ')[0];
  today.appendRow([mapFace(face, spreadsheet), timestamp, mv]);

  reportBatteryLevel(mv, spreadsheet);
}


// test functions
function addSampleDataPoint() {
  addDataPoint("ABCDEFGHIJKL"[Math.floor(Math.random() * 12)], 4200);
}
function testMapFace() {
  var spreadsheet = SpreadsheetApp.getActive();
  mapFace("ABCDEFGHIJKL"[Math.floor(Math.random() * 12)], spreadsheet);
}

function reportLowBattery() {
  var spreadsheet = SpreadsheetApp.getActive();S
  reportBatteryLevel(3699, spreadsheet);
}
function reportCritBattery() {
  var spreadsheet = SpreadsheetApp.getActive();
  reportBatteryLevel(3599, spreadsheet);
}
function reportOKBattery() {
  var spreadsheet = SpreadsheetApp.getActive();
  reportBatteryLevel(3700, spreadsheet);
}

// very GETful API
function doGet(e) {
  var face = e.parameter['face'];
  var mv = parseInt(e.parameter['mv']);
  addDataPoint(face, mv);
  return ContentService.createTextOutput("");
}
