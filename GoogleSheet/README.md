# Integrating TimeTurner data with Google Sheets

These instructions are fairly minimal.

Getting started:
1. Create a new google sheet.

1. Tools → Script editor, which will bring up a new window.

1. Paste the contents of [Code.gs](./Code.gs) into the window.

1. File → Save (or control-s)

If what you just created was a testing spreadsheet, you can test it:

1. In the action bar under the script editor menu bar, choose
   function `addSampleDataPoint` and click the right-pointing-triangle
   "run" button next to it, or Run → Run function → addSampleDataPoint

1. Read it for other sample functions, such as reporting particular
   battery status.

Otherwise, if the sheet is meant to hold real data:

1. Publish → Deploy as web app...

   1. Project Version: **New** "Initial version" (the content of the
      description should be whatever helps you, like the git hash of
      the commit you copied the text from).

   1. Execute the app as: **Me**

   1. Who has access to the app: **Anyone, even anonymous** (the
      TimeTurner isn't set up to do authentication, so it has to
      be anonymous0.

   1. Deploy (for later versions, the button will be "Update")

1. Copy the **Current web app URL:** into TimeTracker.ino

1. Compile and upload the resulting sketch to the TimeTracker,
   with the **SLEEP** face pointing up.

1. On the **Activities** sheet, change the **Name** (column A)
   associated with each **Side** to the specific task that you
   are associating with that face.  Presumably, you have put a
   label on the face on the actual TimeTurner and are just
   copying it to this sheet.

At this point, you can turn the cube. The Summary sheet will turn
yellow when the battery starts to get low, and when it turns red,
plug it in now. If you drain the battery to the point that the unit
quits reporting (3500 mV), you will have to wait up to 5 minutes
after plugging it in before it comes back to life.
