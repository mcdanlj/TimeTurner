/*
 * TimeTurner reporting
 * Uses code from:
 * Heltec ESP32 examples, public domain
 * https://www.mschoeffler.de/2017/10/05/tutorial-how-to-use-the-gy-521-module-mpu-6050-breakout-board-with-the-arduino-uno/
 * (c) Michael Schoeffler 2017, http://www.mschoeffler.de
 * 
 * Information and snippets from:
 * https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/system/sleep_modes.html
 * http://community.heltec.cn/t/heltec-wifi-lora-v2-battery-management/147
 * https://github.com/Heltec-Aaron-Lee/WiFi_Kit_series/issues/6
 * http://web.archive.org/web/20190813003917/https://lukelectro.wordpress.com/2016/08/11/how-to-enable-motion-detection-interrupt-on-mpu6050/
 * https://www.i2cdevlib.com/devices/mpu6050
 * 
 */

#include "Arduino.h"
#include "Wire.h"
#include <WiFi.h>
#include <HTTPClient.h>
#include "driver/rtc_io.h"
#include "heltec.h"
#include "bits/stdc++.h"

typedef struct {
  char name;
  int16_t x;
  int16_t y;
  int16_t z;
} face;

// change this to the URL you want to send face/battery level GETs to
static String URL = "https://example.com/listener";

static face faces[] = { // use debugging statements to populate
  {'A', 300, -300, 15500},
  {'B', 14384, 4348, 6744},
  {'C', 444, 14328, 6548},
  {'D', -13528, 4352, 6284},
  {'E', -8280, -12224, 6592},
  {'F', 9160, -12004, 6480},
  {'G', 14712, -4648, -8196},
  {'H', 10148, 11768, -7744},
  {'I', -7444, 11616, -8940},
  {'J', -13396, -4192, -8548},
  {'K', 848, -14656, -8548},
  {'L', 956, 192, -17452},
};


RTC_DATA_ATTR int bootCount = 0;
RTC_DATA_ATTR char lastFace = '\0';
RTC_DATA_ATTR bool lowBattery = false;

const uint16_t MPU_ADDR = 0x68; // I2C address of the MPU-6050. If AD0 pin is set to HIGH, the I2C address will be 0x69.

int16_t accelerometer_x, accelerometer_y, accelerometer_z; // variables for accelerometer raw data
int16_t gyro_x, gyro_y, gyro_z; // variables for gyro raw data
int16_t temperature, temp_c; // variables for temperature data
const int16_t gyro_threshold = 800;

char tmp_str[7];

const float VOLT_CONVERSION = 0.00232; // determined experimentally with my board, try your own; resistors vary — test near desired shutdown voltage!
const uint16_t MILLIVOLTS = 1000;
const int VBAT = 37;

char* convert_int16_to_str(int16_t i) { // converts int16 to string. Moreover, resulting strings will have the same length in the debug monitor.
  sprintf(tmp_str, "%6d", i);
  return tmp_str;
}

void configure_awake_accelerometer() {
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(0x6B); // PWR_MGMT_1
  Wire.write(0);
  Wire.endTransmission(true);
}
#if 0
// determine how to use zero-motion interrupts and re-write
void configure_accelerometer() {
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(0x1A); // CONFIG
  Wire.write(0x06); // fsync disabled, 5Hz digital low-pass filter (~19ms delay)
  Wire.endTransmission(false);
  Wire.write(0x1F); // MOT_THR
  Wire.write(20); // None
  Wire.endTransmission(false);
  Wire.write(0x20); // MOT_DUR
  Wire.write(40); // None
  Wire.endTransmission(false);
  Wire.write(0x69); // MOT_DETECT_CTRL
  Wire.write(0x15); // None
  Wire.endTransmission(false);
  Wire.write(0x37); // INT_PIN_CFG
  Wire.write(0x10); // INT_RD_CLR: active high, push-pull, 50us pulse, clear int on read
  Wire.endTransmission(false);
  Wire.write(0x38); // INT_ENABLE
  Wire.write(0x00); // None
  Wire.endTransmission(true);
}
void enable_accelerometer_interrupts(){
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(0x38); // INT_ENABLE
  Wire.write(0x01); // DATA_RDY_EN
  Wire.endTransmission(true);
}
#endif
void configure_sleepy_accelerometer() {
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(0x6B); // PWR_MGMT_1
  Wire.write(0x28); // cycle; disable temp
#if 0
  Wire.endTransmission(false);
  Wire.write(0x6C); // PWR_MGMT_2
  Wire.write(0xC7); // 40Hz accelerometer-only low power mode
#endif
  Wire.endTransmission(true);
}
void read_accelerometer_data() {
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(0x3B); // starting with register 0x3B (ACCEL_XOUT_H) [MPU-6000 and MPU-6050 Register Map and Descriptions Revision 4.2, p.40]
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_ADDR, uint8_t(7*2), true);

  accelerometer_x = Wire.read()<<8 | Wire.read(); // reading registers: 0x3B (ACCEL_XOUT_H) and 0x3C (ACCEL_XOUT_L)
  accelerometer_y = Wire.read()<<8 | Wire.read(); // reading registers: 0x3D (ACCEL_YOUT_H) and 0x3E (ACCEL_YOUT_L)
  accelerometer_z = Wire.read()<<8 | Wire.read(); // reading registers: 0x3F (ACCEL_ZOUT_H) and 0x40 (ACCEL_ZOUT_L)
  temperature = Wire.read()<<8 | Wire.read(); // reading registers: 0x41 (TEMP_OUT_H) and 0x42 (TEMP_OUT_L)
  gyro_x = Wire.read()<<8 | Wire.read(); // reading registers: 0x43 (GYRO_XOUT_H) and 0x44 (GYRO_XOUT_L)
  gyro_y = Wire.read()<<8 | Wire.read(); // reading registers: 0x45 (GYRO_YOUT_H) and 0x46 (GYRO_YOUT_L)
  gyro_z = Wire.read()<<8 | Wire.read(); // reading registers: 0x47 (GYRO_ZOUT_H) and 0x48 (GYRO_ZOUT_L)
  // the following equation was taken from the documentation [MPU-6000/MPU-6050 Register Map and Description, p.30]
  //temp_c = temperature/340.00+36.53;

  // print out accelerometer data in faces format
  Serial.printf("{'', %d, %d, %d},\n", accelerometer_x, accelerometer_y, accelerometer_z);
#if 0
  Serial.print("aX = "); Serial.print(convert_int16_to_str(accelerometer_x));
  Serial.print(" | aY = "); Serial.print(convert_int16_to_str(accelerometer_y));
  Serial.print(" | aZ = "); Serial.print(convert_int16_to_str(accelerometer_z));
  Serial.print(" | tmp = "); Serial.print(temp_c);
  Serial.print(" | gX = "); Serial.print(convert_int16_to_str(gyro_x));
  Serial.print(" | gY = "); Serial.print(convert_int16_to_str(gyro_y));
  Serial.print(" | gZ = "); Serial.print(convert_int16_to_str(gyro_z));
  Serial.println();
#endif
}
bool accelerometer_gryro_unstable() {
  bool unstable = max(max(abs(gyro_x), abs(gyro_y)), abs(gyro_z)) > gyro_threshold;
  if (unstable) {
    delay(100);
  }
  return unstable;
}
void configure_battery_adc() {
  Heltec.begin(false, false, false); // not clear what setting in here is required to read battery voltage
  analogSetCycles(8);
  analogSetSamples(1);
  analogSetClockDiv(1);
  analogSetPinAttenuation(VBAT,ADC_11db);
  adcAttachPin(VBAT);
}
uint16_t read_battery_millivolts() {
  adcStart(VBAT);
  while(adcBusy(VBAT));
  Serial.println(analogRead(VBAT));
  uint16_t mv  =  analogRead(VBAT)*VOLT_CONVERSION*MILLIVOLTS;
  adcEnd(VBAT);
  // can I turn something off now?
  // FIXME
  return mv;
}

char find_face() {
  char thisFace = '\0';
  int max_dp = 0;
  static int length = sizeof faces / sizeof faces[0];
  for (int i=0; i<length; i++) {
    int dp = accelerometer_x * faces[i].x + accelerometer_y * faces[i].y + accelerometer_z * faces[i].z;
    if (dp > max_dp) {
      thisFace = faces[i].name;
      max_dp = dp;
    }
  }
  return thisFace;
}
bool face_changed() {
  char currentFace = find_face();
  bool changed = currentFace != lastFace;
  if (changed) {
    lastFace = currentFace;
  }
  return changed;
}

void print_wakeup_reason(){
  esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch(wakeup_reason)
  {
    case ESP_SLEEP_WAKEUP_EXT0 : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
    default : Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); break;
  }
}

void connect_wifi() {
  //WiFi.begin("your_essid", "your_passphrase"); // only need this once; the ESP32 remembers
  WiFi.begin();

  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
}
void get(String url) {
  HTTPClient http;

  http.begin(url);
  int httpCode = http.GET();

  if (httpCode > 0) {
    String payload = http.getString();
    Serial.println(httpCode);
    Serial.println(payload);
  } else if (httpCode == 302) {
    // In my case, this redirect needs to not be followed!
    Serial.println("Ignoring redirect");
  } else {
   Serial.println("Error on HTTP request");
  }

  http.end();

}
void report_face(char face, uint16_t vbat) {
  connect_wifi();
  get(String(URL) + "?face=" + String(face) + "&mv=" + String(vbat));
  WiFi.disconnect();
}

void setup(){
  //rtc_gpio_deinit(GPIO_NUM_34);
  configure_battery_adc();
  uint16_t vbat = read_battery_millivolts();

  if (!lowBattery) {
    Serial.begin(115200);
    Wire.begin(GPIO_NUM_21, GPIO_NUM_22, 100000); // SDA, SCL, frequency

    ++bootCount;
    Serial.println("Boot number: " + String(bootCount));

    //Print the wakeup reason for ESP32
    //print_wakeup_reason();
    configure_awake_accelerometer();
    //configure_accelerometer();
    do {
      read_accelerometer_data();
    } while (accelerometer_gryro_unstable());

    // report upward face here
    if (face_changed() || bootCount % 60 == 1) { // make sure always reported if bootCount == 1
      Serial.println("Face: " + String(lastFace));
      report_face(lastFace, vbat);
    } else {
      Serial.println("Face unchanged: " + String(lastFace));
    }
    Serial.println("VBAT: " + String(vbat));
  }

  if (vbat > 3500) {
    // Set to wake up only if sufficient voltage; otherwise sleep until recharged
    //rtc_gpio_pulldown_en(GPIO_NUM_34);
    //esp_sleep_enable_ext0_wakeup(GPIO_NUM_34,1);

    // check orientation
    //esp_sleep_enable_timer_wakeup(60*1000000);
    esp_sleep_enable_timer_wakeup(10*1000000);

    //enable_accelerometer_interrupts();
  } else {
    lowBattery = true;
    // disable everything possible to reduce sleep power, but still need to eventually recover; check back in 5 minutes for power
    esp_sleep_enable_timer_wakeup(300*1000000);
  }

  esp_deep_sleep_start();
}

void loop(){
  // not reached
}
