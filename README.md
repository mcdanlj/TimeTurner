# TimeTurner

This is a time-recording dodecahedron. It senses which side is up, and
reports via HTTP when that changes. It also reports battery status
at every face change and periodically (approximately every ten minutes).

I printed in PETG. The lid is a precise snap-fit that takes a lot of
pressure to seat and is not easily removed, and will not fit at all
unless the print is precise.

Status: I'm sharing what is _just barely_ working for me. I have
more things I would like to change to improveit , and am not sure
if/when I'll do any of that work. The code is primarily quickly
pasted together from examples and is ugly.

My use case is [reporting to a google sheet](GoogleSheet/README.md)
via google app scripts webapp integration. I did the least workx
possible for this to work.

## BOM

* About 200g of PETG filament

* [Heltec ESP32 dev board with integrated LiPo charge controller](https://www.amazon.com/gp/product/B076KJZ5QM)

* [852540 1000mAh LiPo cell with JST1.25 1.25mm connector for dev board](https://www.amazon.com/gp/product/B07CXNQ3ZR)

* [MPU-6050 breakout board](https://www.amazon.com/gp/product/B00LP25V1A)

* [USB-C to micro-USB adapter](https://www.amazon.com/gp/product/B07VBV1PY5)
  (The units I received were a very tight fit into the micro-USB slot
  on the dev board, which is fine here because I want it to be
  permanent, but I'd be careful plugging into a phone...)

* 2 M3 screws, 8-12mm length, counter-sink head preferred.

* Labels for sides

* Hookup wire (I used 28AWG stranded)

## Instructions

Currently, interrupt handling has not been implemented, and the MPU6050
documentation does not describe how to enable interrupts on motion or
on lack of motion, both of which are supposed to be features of the
hardware. Some reverse engineering has been done. A quick attempt was
not successful.

I might later get around to trying to make it work, so I connected the
MPU to VCC, so that it has the ability later to generate interrupts.
If you aren't going to try to generate interrupts, you might instead
connect to Vext so that you can turn it on only momentarily to get a
reading, and then turn it off. This could substantially increase
battery life.  (The sketch does not support Vext management for now.)

Solder 15cm wires from dev board to breakout board.
* 3V3 to VCC (alternatively Vext if no plans to try interrupts)
* GND to GND
* 22 to SCL
* 21 to SDA
* 34 to INT (currently unused, leave off if using Vext)

Modify the included sketch to include your desired endpoint and
WiFi credentials, then upload it.  Use the serial terminal to
validate that the MPU is functional.

Once the ESP32 has associated with your access point, it will remember
the ESSID and passphrase and you can remove them from the code.

After verifying that the MPU is functional, if using VCC, consider
desoldering the LED from the breakout board to reduce battery load.
There is no mechanism to disconnect the battery, so if you do not
recharge, you will eventually discharge the battery to its rather
low 2.75V low-voltage cut-off; 3V is a recomended minimum for long
battery life on LiPo cells.

Print in PETG.  Use M3 screws to attach breakout board to pillar
with screw holes, with the attached wires toward the side of the
case, away from the battery/CPU mounting features. The holes are
sized to not require tapping.  Screw in tight enough that the MPU
is firmly seated to the post without breaking anything; it should
not wiggle if you push it with your finger.

If USB-C adapter does not fit in hole in side C, carefully file with
a round ("rat-tail") file until the USB-C connector barely fits.

Insert the USB-C adapter into the case from the front, threading
the micro-usb end of the connector through the hole, then the USB-C
end until it is flush against the stop at the back.

Attach the micro-USB end of the cable to the dev board, the LiPo
cell to the bottom of the dev board, and slide the dev board into
the slot. If it's a loose fit, put a layer or two of thin tape
like kapton tape in one or both slots until it is a snug fit.
Slide the battery in between the battery holder tangs, and tape
it securely in place across the sides and top.

Attach a USB-C cable, bring up a serial monitor, and set the cube on
each face. (You'll need to use the edge of a table to set it flat on
face C while the cable is attached.)  It will print out a vector for
each face, as well as the face name. Change the example face vectors
at the top of the sketch if you need to, and re-upload the sketch.

Snap the top into place. I had to work around from edge to edge
and it took a lot of force to make it snap in, and then was
very solid. The rectangular hole in the top is intended to
allow you to use a large screwdriver to remove the top later
for tasks like battery replacement. Unsure how many cycles the
lid fit can take before failing; this is an experiement.

At this point, you'll want to keep the unit charged.  It always
draws down the battery until the battery undervoltage protection
kicks in. If you plan to leave it alone for a month, you probably
want to leave it plugged in rather than draining it all the way.
I have it set to go into deep sleep and stay there for five minutes
at a time and stop reading or reporting MPU status once it reaches
3500 mV. I expect to charge it when it discharges into the
3600-3700 mV range, probably once every several days.  Note that
the reset button is not exposed, so if you need to reset it, you
will have to pop the lid off. (I may add a recessed reset button
if I make a new revision of the hardware.)

You can put labels on the sides to write names. If you try to
write directly on the PETG it will probably fill in the cracks
and never come out no matter how hard you try.

If you want to track with Google Sheets, you have the option to
start with a blank sheet and the [script I created](GoogleSheet/README.md).
No promises that it doesn't eat all your data.
